*** Settings ***
Library    SeleniumLibrary    

*** Variables ***
${url}=    http://tutorialsninja.com/demo/index.php?route=common/home
${BROWSER}=    gc
&{Logins}    firstname=Jack    lastname=Daniel    email=32504092021@mail.com    telephone=1234567891    password=qwerty123

*** Keywords ***
remplir le formulaire d inscription tutorialsninja
    [Documentation]    Pour ouvrir forme de s'inscrire pour tutorialsninja site
    Open Browser    ${url}    ${BROWSER}
    Maximize Browser Window
    # Click Link    //span[@class="caret"]
    # Click Link    //a[contains(@href,'http://tutorialsninja.com/demo/index.php?route=account/account')]
    Click Link    //div[@id='top-links']/ul/li/a[contains(@href,'http://tutorialsninja.com/demo/index.php?route=account/account')]
    Sleep    1s
    Click Link    //a[@href="http://tutorialsninja.com/demo/index.php?route=account/register"]
    Sleep    1s
    # Remplissez les champes "Your Personal Details"
    Input Text    css=input[id='input-firstname']    ${Logins}[firstname]
    Input Text    css=input[name='lastname']    ${Logins}[lastname]
    Input Text    css=input[name='email']    ${Logins}[email]
    Input Text    css=input[name='telephone']    ${Logins}[telephone]
    Input Password    css=input[name='password']    ${Logins}[password]
    Input Password    css=input[name='confirm']    ${Logins}[password]
    # Selectionner checkbox "I have read and agree to the Privacy Policy"
    Select Checkbox    //input[@name='agree'][@value='1']
    Sleep    1s
    # Dans la section "Newsletter", sur la ligne "Subscribe" cliquez sur le bouton radio "Yes ".
    Select Radio Button    newsletter    1
    Sleep    1s
    # Appuyez sur le bouton "Continue"
    Click Button    css=input[type='submit']

*** Test Cases ***
TC_01_EX02.01_Enregistrement_utilisateur_newsletter_nominal
    [Documentation]    Cas de test EX02.01 Enregistrement de l'utilisateur en optant pour l'abonnement à la Newsletter(scénario nominal) dans l'environnement réel
    remplir le formulaire d inscription tutorialsninja
    Sleep    2s
    # Verification "Your Account Has Been Created!" apparaît.
    Page Should Contain Element    //div/h1[contains(text(), 'Your Account Has Been Created!')]
    Sleep    1s
    # Appuyez sur le bouton "Continue"
    Click Element    //div/a[contains(text(), 'Continue')]    
    # Verification une nouvelle page apparaît, intitulée  "Your Store"
    Page Should Contain Element    //div/h1/a[contains(text(), 'Your Store')]
    # Capture d'écran d'exécution réussie de cas de test
    Capture Page Screenshot    ./Screenshot/login_page.png
    Log To Console    ce test est exucuter par %{username} dans le OS %{os}
    Sleep    1s
    Close Browser
    
