*** Settings ***
Documentation    Projet Automatisation Alfresco
Library    SeleniumLibrary   
Library    DateTime    
Library    String    
Library    Dialogs    
Library    OperatingSystem    
Library    Collections  

*** Variables ***
${vURL}    http://tutorialsninja.com/demo/index.php?route=common/home 
${vBrowser}    chrome
${vProduit}    Samsung
${vTypeProduit}    Samsung SyncMaster 941BW
${vTitle}    Your Store
${TIMEOUT}    2s

*** Test Cases ***
Commander un produit
    Log     Summary: Commander un produit - scenario nominal
    Log     Connexion
    # Ouvrir le navigateur en précisant l'URL et le navigateur
    Open Browser     ${vURL}     ${vBrowser}
    # Maximiser la fenêtre du navigateur
    Maximize Browser Window
    # Vérification du titre de la page
    Title Should Be     ${vTitle}
    #Saisir le nom du produit recherche
    Input text     //input[@name='search']    ${vProduit}
    sleep     ${TIMEOUT}
    #cliquer sur le bouton de recherche (image Loupe)
    Click Element     //span[@class='input-group-btn']/button[1]
    sleep     ${TIMEOUT}
    # Vérification du resultat  de la recherche: titre
    Wait Until Element Is Visible    //h1[(text()='Search - ${vProduit}')]
    sleep     ${TIMEOUT}
    # cliquer sur le Samsung SyncMaster 941BW
    Click Element    //img[@title='${vTypeProduit}']
    sleep     ${TIMEOUT}
    #Cliquez sur le bouton "Add to cart"
    Click Button    //button[@id='button-cart']
    sleep     4s
    # Une bannière verte apparaît, indiquant "Success: You have added Samsung SyncMaster 941BW to your shopping cart!".
    Page Should Contain Element    //div[@id='product-product']/div[1]/a[text()='${vTypeProduit}']
    sleep     ${TIMEOUT}
    #Cliquer sur le titre Your Store pour revenir a la page principale
    Click Element    //a[contains(text(),'Your Store')]
    sleep     ${TIMEOUT}
     # Fermer le navigateur
    Close Browser