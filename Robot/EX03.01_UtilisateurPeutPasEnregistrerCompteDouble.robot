*** Settings ***
Library    SeleniumLibrary   

 
*** Variables ***
${url}=    http://tutorialsninja.com/demo/index.php?route=common/home
${BROWSER}=    gc
&{Logins}    firstname=Jack    lastname=Daniel    email=jackdaniel@mail.com    telephone=1234567891    password=qwerty123

*** Keywords ***

remplir le formulaire d enregistrement tutorialsninja
    [Documentation]    Pour ouvrir forme de sinscrire pour tutorialsninja site
	#Ouvrir le navigateur Chrome
    Open Browser    ${url}    ${BROWSER}
    Maximize Browser Window
    # Trouvez le bouton "My Account" dans le coin supérieur droit et cliquez dessus.
    Click Link    //div[@id='top-links']/ul/li/a[contains(@href,'http://tutorialsninja.com/demo/index.php?route=account/account')]
    Sleep    1s
    #Pointez le curseur de la souris sur le champ "Register" et cliquez sur
    Click Link    //a[@href="http://tutorialsninja.com/demo/index.php?route=account/register"]
    Sleep    1s
    #Dans la section "Your Personal Details" remplissez les champs marqués d'un "*". 
    Input Text    css=input[id='input-firstname']    ${Logins}[firstname]
    Input Text    css=input[name='lastname']    ${Logins}[lastname]
    #Dans le champ "EMAIL", indiquez une adresse e-mail existante "jackdaniel@mail.com".
    Input Text    css=input[name='email']     ${Logins}[email]
    Input Text    css=input[name='telephone']    ${Logins}[telephone]
    #Dans la section "Your Password" remplissez les champs marqués d'un "*"
    Input Password    css=input[name='password']    ${Logins}[password]
    #Entrez à nouveau le mot de passe
    Input Password    css=input[name='confirm']    ${Logins}[password]
    # Selectionner checkbox "I have read and agree to the Privacy Policy"
    Select Checkbox    //input[@name='agree'][@value='1']
    Sleep    1s
    #Appuyez sur le bouton "Continue".
    Click Button    css=input[type='submit']
    Sleep    2s    
    

*** Test Cases ***
TC01_EX03.01_UtilisateurPeutPasEnregistrerCompteDouble
    [Documentation]    Cas de test dans lenvironnement reel
    remplir le formulaire d enregistrement tutorialsninja
    Sleep    2s
    #Vérifiez si la page requise du site est ouverte
    Page Should Contain Element    //body/div[@id='account-register']/div[1]    
    # Capture d'ecran d'execution reussie de cas de test
    Capture Page Screenshot    ./Screenshot/login_page.png
    Log To Console    ce test est exucuter par %{username} dans le OS %{os}
    Sleep    1s







