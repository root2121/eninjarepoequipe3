*** Settings ***
Documentation    Projet Automatisation Alfresco
Library    SeleniumLibrary   
Library    DateTime    
Library    String    
Library    Dialogs    
Library    OperatingSystem    
Library    Collections  

*** Variables ***
${vURL}    http://tutorialsninja.com/demo/index.php?route=common/home 
${vBrowser}    chrome
${vProduit}    SamsungNone
${vTitle}    Your Store
${TIMEOUT}    2s

*** Test Cases ***
Info pour le produit indisponible
    Log     Summary: Informe l utilisateur de l indisponibilite du produit - scenario nominal
    Log     Connexion
    # Ouvrir le navigateur en précisant l'URL et le navigateur
    Open Browser     ${vURL}     ${vBrowser}
    # Maximiser la fenêtre du navigateur
    Maximize Browser Window
    # Vérification du titre de la page
    Title Should Be     ${vTitle}
    #Saisir le nom du produit recherche
    Input text     //input[@name='search']    ${vProduit}
    sleep     ${TIMEOUT}
    #Click sur le bouton de recherche (image Loupe)
    Click Element     //span[@class='input-group-btn']/button[1]
    sleep     ${TIMEOUT}
    # Vérification du resultat  de la recherche: titre
    Wait Until Element Is Visible    //h1[(text()='Search - ${vProduit}')]
    # Vérification du resultat  de la recherche: text
    Page Should Contain Element    //p[contains(text(),'There is no product that matches the search criter')]
    sleep     ${TIMEOUT}
    Log To Console    test reussi
     # Fermer le navigateur
    Close Browser