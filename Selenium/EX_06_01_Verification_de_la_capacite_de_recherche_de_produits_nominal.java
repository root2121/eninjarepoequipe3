package E_Com;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EX_06_01_Verification_de_la_capacite_de_recherche_de_produits_nominal {
	ChromeDriver driver;

	@Test
	public void Samsung() throws InterruptedException {
		// � l'aide du navigateur Chrome, entrez dans la page d'accueil du site.
		driver.get("http://www.tutorialsninja.com/demo/");
		// Trouvez le bouton "My Account" dans le coin sup�rieur droit et cliquez
		// dessus.
		driver.findElement(By.xpath("//span[text()='My Account']")).click();
		Thread.sleep(4000);
		//// Pointez le curseur de la souris sur le champ "Login" et cliquez sur
		driver.findElement(By.linkText("Login")).click();
		// Dans la sous-fen�tre "Returning Customer", remplissez le champ "E-Mail
		// Address"
		driver.findElement(By.id("input-email")).sendKeys("jackdaniel@gmail.com");
		// Dans la sous-fen�tre "Returning Customer", remplissez le champ "Password"
		driver.findElement(By.name("password")).sendKeys("qwerty123");
		// Cliquez sur le bouton "Login"
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		// En haut du site, dans le champ "recherche", tapez le nom du produit.
		// .sendKeys("samsung");
		driver.findElement(By.name("search")).sendKeys("samsung");
		// Cliquez sur le bouton "Search"
		driver.findElement(By.xpath("//*[@id=\"search\"]/span/button")).click();
		Thread.sleep(4000);
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("***avant tous les testes");
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();

	}

	@AfterClass
	public void afterClass() {
		System.out.println("***apres tous les testes");
		driver.close();
	}

}
