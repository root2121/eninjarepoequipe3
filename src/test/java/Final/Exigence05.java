package Final;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Exigence05 {
	ChromeDriver driver;

	@Test
	public void ForgotPassword() throws InterruptedException {
		driver.get("http://www.tutorialsninja.com/demo/");

		driver.findElement(By.xpath("//span[text()='My Account']")).click();
		Thread.sleep(4000);
		driver.findElement(By.linkText("Login")).click();
		driver.findElement(By.linkText("Forgotten Password")).click();

		driver.findElement(By.id("input-email")).sendKeys("jackdaniel@gmail.com");

		driver.findElement(By.xpath("//input[@type='submit']")).click();

	}

	@BeforeClass
	public void beforeClass() {
		// System.out.println("***avant tous les testes");
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();

	}

	@AfterClass
	public void afterClass() {
		// System.out.println("***apres tous les testes");
		// driver.close();
	}

}
