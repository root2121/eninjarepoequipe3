package Final;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Exigence03 {
	ChromeDriver driver;

	@Test
	public void Warming() throws InterruptedException {
		driver.get("http://www.tutorialsninja.com/demo/");

		driver.findElement(By.xpath("//span[text()='My Account']")).click();
		Thread.sleep(4000);
		driver.findElement(By.linkText("Register")).click();
		driver.findElement(By.name("firstname")).sendKeys("Jack");
		driver.findElement(By.name("lastname")).sendKeys("Daniel");
		driver.findElement(By.id("input-email")).sendKeys("jackdaniel@");
		driver.findElement(By.name("telephone")).sendKeys("1234567891");

		// password
		driver.findElement(By.name("password")).sendKeys("qwerty123");
		driver.findElement(By.id("input-confirm")).sendKeys("qwerty123");
		
		driver.findElement(By.xpath("//input[@type='submit']")).click();

		

	}

	@BeforeClass
	public void beforeClass() {
		// System.out.println("***avant tous les testes");
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();

	}

	@AfterClass
	public void afterClass() {
		// System.out.println("***apres tous les testes");
		// driver.close();
	}

}
